package id.sch.smktelkom_mlg.lesson7_1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;


/**
 * Gets the data from the ContentProvider and shows a series of flash cards.
 */


public class MainActivity extends AppCompatActivity {


    // The current state of the app

    private final int STATE_HIDDEN = 0;
    private final int STATE_SHOWN = 1;


    // This state is when the word definition is hidden and clicking the button will therefore

    // show the definition
    private int mCurrentState;


    // This state is when the word definition is shown and clicking the button will therefore

    // advance the app to the next word
    private Button mButton;

    @Override

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);


        // Get the views

        mButton = findViewById(R.id.button_next);

    }


    /**
     * This is called from the layout when the button is clicked and switches between the
     * <p>
     * two app states.
     *
     * @param view The view that was clicked
     */

    public void onButtonClick(View view) {


        // Either show the definition of the current word, or if the definition is currently

        // showing, move to the next word.

        switch (mCurrentState) {

            case STATE_HIDDEN:

                showDefinition();

                break;

            case STATE_SHOWN:

                nextWord();

                break;

        }

    }


    public void nextWord() {


        // Change button text

        mButton.setText(getString(R.string.show_definition));


        mCurrentState = STATE_HIDDEN;


    }


    public void showDefinition() {


        // Change button text

        mButton.setText(getString(R.string.next_word));


        mCurrentState = STATE_SHOWN;


    }


}
