package id.sch.smktelkom_mlg.lesson7_1;

import android.content.ContentResolver;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;


/**
 * Gets the data from the ContentProvider and shows a series of flash cards.
 */


public class Main2Activity extends AppCompatActivity {


    // COMPLETED (3) Create an instance variable storing a Cursor called mData

    // The data from the DroidTermsExample content provider

    private final int STATE_HIDDEN = 0;


    // The current state of the app
    private final int STATE_SHOWN = 1;
    private Cursor mData;


    // This state is when the word definition is hidden and clicking the button will therefore

    // show the definition
    private int mCurrentState;


    // This state is when the word definition is shown and clicking the button will therefore

    // advance the app to the next word
    private Button mButton;

    @Override

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);


        // Get the views

        mButton = findViewById(R.id.button_next);


        //Run the database operation to get the cursor off of the main thread

        // COMPLETED (5) Create and execute your AsyncTask here

        new WordFetchTask().execute();


    }


    /**
     * This is called from the layout when the button is clicked and switches between the
     * <p>
     * two app states.
     *
     * @param view The view that was clicked
     */

    public void onButtonClick(View view) {


        // Either show the definition of the current word, or if the definition is currently

        // showing, move to the next word.

        switch (mCurrentState) {

            case STATE_HIDDEN:

                showDefinition();

                break;

            case STATE_SHOWN:

                nextWord();

                break;

        }

    }


    public void nextWord() {


        // Change button text

        mButton.setText(getString(R.string.show_definition));


        mCurrentState = STATE_HIDDEN;


    }


    public void showDefinition() {


        // Change button text

        mButton.setText(getString(R.string.next_word));


        mCurrentState = STATE_SHOWN;


    }

    // COMPLETED (1) Create AsyncTask with the following generic types <Void, Void, Cursor>

    // COMPLETED (2) In the doInBackground method, write the code to access the DroidTermsExample

    // provider and return the Cursor object

    // COMPLETED (4) In the onPostExecute method, store the Cursor object in mData


    // Use an async task to do the data fetch off of the main thread.

    public class WordFetchTask extends AsyncTask<Void, Void, Cursor> {


        // Invoked on a background thread

        @Override

        protected Cursor doInBackground(Void... params) {

            // Make the query to get the data


            // Get the content resolver

            ContentResolver resolver = getContentResolver();


            // Call the query method on the resolver with the correct Uri from the contract class

            Cursor cursor = resolver.query(DroidTermsExampleContract.CONTENT_URI,

                    null, null, null, null);

            return cursor;

        }


        // Invoked on UI thread

        @Override

        protected void onPostExecute(Cursor cursor) {

            super.onPostExecute(cursor);


            // Set the data for MainActivity

            mData = cursor;

        }

    }


}